const neo4j = require('neo4j-driver').v1;

const driver = neo4j.driver(process.env.NEO4J_DROGERIJE_URL, neo4j.auth.basic(process.env.NEO4j_USERNAME, process.env.NEO4J_PWD));

module.exports = {
    driver: driver
}