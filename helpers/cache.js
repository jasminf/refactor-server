const NodeCache = require("node-cache");
const cache = new NodeCache();

const setValue = (key, value) => {
    return cache.set(key, value);
}

const getValue = (key) => {
    return cache.get(key);
}

const deleteValue = (key) => {
    return cache.del(key);
}

module.exports = {
    setValue: setValue,
    getValue: getValue,
    deleteValue: deleteValue
}