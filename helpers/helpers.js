const fs = require('fs');
const endOfLine = require('os').EOL;

const getTodaysTimeStamp = () => {
    let date = new Date();

    let day = date.getDate().toString();
    let month = (date.getMonth() + 1).toString();
    let year = date.getFullYear().toString();

    if (day.length === 1) day = '0' + day;

    if (month.length === 1) month = '0' + month;

    year = year.substr(2, 2);

    return day + month + year;
};

const prepareTimeStamp = order => {
    let date = new Date(order.pickupDate);

    let day = date.getDate().toString();
    let month = (date.getMonth() + 1).toString();
    let year = date.getFullYear().toString();

    if (day.length === 1) day = '0' + day;

    if (month.length === 1) month = '0' + month;

    year = year.substr(2, 2);

    return day + month + year;
};

const prepareTimeStampWithoutOrder = () => {
    let date = new Date();

    let day = date.getDate().toString();
    let month = (date.getMonth() + 1).toString();
    let year = date.getFullYear().toString();

    if (day.length === 1) day = '0' + day;

    if (month.length === 1) month = '0' + month;

    year = year.substr(2, 2);

    return day + month + year;
};

const prepareOrderId = orderId => {
    orderId = orderId.toString();

    if (orderId.length === 1) orderId = '00' + orderId;
    else if (orderId.length === 2) orderId = '0' + orderId;

    return orderId;
};

const log = infoMessage => {
    if (!infoMessage) {
        console.log('logInfo:: No infoMessage message!');
        return;
    }
    const timestamp = getTodaysTimeStamp();
    const logDirectory = process.env.LOGS_DIRECTORY_DESTINATION || null;
    const fileName = +timestamp + '_logs' + '_info' + '.txt';

    const mesageDate = new Date().toDateString() + ' --> ' + new Date().toLocaleTimeString();
    const message = mesageDate + ': ' + infoMessage + endOfLine;

    if (logDirectory && !fs.existsSync(logDirectory)) fs.mkdirSync(logDirectory);

    const appendFileName = logDirectory ? logDirectory + fileName : fileName;

    fs.appendFile(appendFileName, message, err => {
        if (err) console.log(err);
    });
};

const logException = (error, global = false, additionalData = null) => {
    console.log(error);
    let timestamp = getTodaysTimeStamp();
    let logsDirectory = process.env.LOGS_DIRECTORY_DESTINATION || null;
    let fileName = +timestamp + '_logs' + '.txt';
    let exception =
        new Date().toDateString() +
        ' --> ' +
        new Date().toLocaleTimeString() +
        endOfLine +
        '--------------------------------------------------' +
        endOfLine;
    exception += 'Name: ' + (error.name || '') + endOfLine;
    exception += 'Message: ' + (error.message || '') + endOfLine;

    if (global)
        exception +=
            'Stack: ' +
            (error.extensions && error.extensions.exception
                ? error.extensions.exception.stacktrace
                : '') +
            endOfLine;
    else exception += 'Stack: ' + (error.stack || '') + endOfLine;

    if (additionalData) {
        exception += 'Additional data: ';
        for (let key in additionalData) {
            if (additionalData.hasOwnProperty(key))
                exception += key + ':' + (additionalData[key] || '').toString() + '  ';
        }
    }

    exception += endOfLine + endOfLine;

    if (logsDirectory && !fs.existsSync(logsDirectory)) fs.mkdirSync(logsDirectory);

    fs.appendFile(logsDirectory ? logsDirectory + fileName : fileName, exception, err => {
        if (err) console.log(err);
    });
};
const dayAsString = dayIndex => {
    let weekdays = new Array(7);
    weekdays[0] = 'Nedelja';
    weekdays[1] = 'Ponedeljek';
    weekdays[2] = 'Torek';
    weekdays[3] = 'Sreda';
    weekdays[4] = 'Četrtek';
    weekdays[5] = 'Petek';
    weekdays[6] = 'Sobota';
    return weekdays[dayIndex];
};

module.exports = {
    getTodaysTimeStamp: getTodaysTimeStamp,
    prepareOrderId: prepareOrderId,
    prepareTimeStamp: prepareTimeStamp,
    prepareTimeStampWithoutOrder: prepareTimeStampWithoutOrder,
    logException: logException,
    dayAsString: dayAsString,
    log,
};
