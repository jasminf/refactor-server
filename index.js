require('dotenv').config();
const express = require('express');
const bodyParser = require('body-parser');
const uuidv1 = require('uuid/v1');

const apolloServer = require('./graphql/server');
const cacheModule = require('./helpers/cache');


const app = express();
const jsonParser = bodyParser.json({ limit: '2mb' });
app.use(jsonParser);


apolloServer.applyMiddleware({
    app,
    cors: {
        credentials: true,
        origin: true,
    },
});

app.use(function(error, req, res, next) {
    helpers.logException(error, false);
    res.json({ message: error.message });
});

const serverPort = process.env.SERVER_PORT || 4000;

app.on('error', function(err) {
    console.log(err)
}).listen({ port: serverPort }, () => {
    let version = uuidv1();

    cacheModule.deleteValue('apiVersion');
    cacheModule.setValue('apiVersion', version);

    console.log(`API version: ${version}`);
    console.log(`Server ready at ${serverPort}`);
});
