const { neo4jgraphql } = require('neo4j-graphql-js');
const drivers = require('../neo4j');

const resolvers = {
    Query: {
        getAppConfiguration(root,args,context,info) {
            const session = drivers.driver.session();
            const query = `MATCH (a:Application)-[:CONTAINS]->(v:View)-[cc:CONTAINS]->(c:Component)
            WITH c
            CALL custom.expandViewToTree(c) YIELD tree
            RETURN apoc.convert.toJson(tree) as view`

            return session.run(query).then(res => {
                session.close();
                return res.records[0].get("view")
            })
        }
    }
}


module.exports = resolvers;