const { gql } = require('apollo-server');


const typeDefs = gql `
    type Query {
        getAppConfiguration: String
    }
`
module.exports = typeDefs;