const { ApolloServer, ApolloError } = require('apollo-server-express');
const typeDefs = require('./schema');
const resolvers = require('./resolvers');
const drivers = require('../neo4j');
const helpers = require('../helpers/helpers');
const cacheModule = require('../helpers/cache');

const ignoreOperations =
    'getApiVersion,appConfiguration,saveSession,storeEvents,getUserData, getStores,pageAndEventsSave, getAppConfiguration';
const whiteList = 'getAppConfiguration,getStores';

const apolloServer = new ApolloServer({
    typeDefs,
    resolvers: resolvers,
    mocks: false,
    context: ({ req, res }) => {
        // This property will be set and passed on every request and we can use it for authentication/authorization purposes
        //const authorization = req.headers.authorization || null;

        const driver = drivers.driver; // Injecting neo4j database driver so it can be used by neo4j-graphql-js
        //const apiVersion = req.headers.apiversion || null; // Client will send version of the API
        //const mobileSecret = req.headers.mobilesecret || null;
        const request = req;
        const response = res;
        //const hasCookie = request.cookies[process.env.COOKIE];
        // We should handle api version here and stop the request if it is not the same!
        if (!ignoreOperations.includes(req.body.operationName)) {
            return { driver, response, request };
        }
      
    },
    formatError: error => {
        // Log exceptions to database before we return it to the caller
        helpers.logException(error, true);

        if (error.extensions && error.extensions.code === 1020) {
            // Request didn't had apiVersion header or apiVersion value from header is not same as apiVersion on API backend service
            return { message: cacheModule.getValue('apiVersion'), statusCode: 1020 };
        } else if (error.extensions && error.extensions.code === 1030) {
            // Request failed as there is no cookie in request.
            return { message: 'Authorization failed', statusCode: 1030 };
        } else return new Error('Internal Server Error');
    },
});

module.exports = apolloServer;

